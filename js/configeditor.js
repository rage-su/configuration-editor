/*--

 *	configeditor.js
The RAGE Configuration Editor.

 * Copyright 2017 Sofia University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
This project has received funding from the European Union’s Horizon
2020 research and innovation programme under grant agreement No 644187.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

 
var knownTypes = ['date', 'paragraph', 'text', 'url', 'group', 'list', 'integer', 'float'];

function createXML(fakeData) {
	var xml = '';

	// metadata opening tag
	xml += '<?xml version="1.0" encoding="UTF-8"?>\n';

	xml += '<' + config[1].name + '>\n';

	// create fake data
	if (fakeData)
	{
		if (config[1].type=='group')
		{
			xml += createXMLComplexElement('	', config.indexOf(config[1].group));
		}
		else
			xml += createXMLElement('	', config[1],true);
	}
	
	// metadata closing tag
	xml += '</' + config[1].name + '>\n';

	return xml;
}

function createXMLComplexElement(tabs, from) {
	var xml = '';
	for (var i = from + 1; config[i]instanceof Object; i++) {
		var elem = config[i];
		var count = 1;
		if (elem.occurs == '0u' || elem.occurs == '1u')
			count += Math.round(3 * Math.random());

		// generate 'count' instances
		while (count--)
			xml += createXMLElement(tabs, elem);
	}
	return xml;
}

function createXMLElement(tabs, elem, skipTag) {
	var xml = '';

	function hasTag(tag)
	{
		return skipTag?'':tag;
	}
	
	if (elem.type == 'text') {
		xml += tabs + hasTag('<' + elem.name + '>') + randomPhrase() + hasTag('</' + elem.name + '>')+'\n';
	}
	else if (elem.type == 'paragraph') {
		xml += tabs + hasTag('<' + elem.name + '>') + randomPhrase(15) + hasTag('</' + elem.name + '>')+'\n';
	} else if (elem.type == 'url') {
		xml += tabs + hasTag('<' + elem.name + '>')+'http://' + randomWord() + '.' + randomSyll(['bg', 'en', 'it', 'de', 'fr', 'ro', 'no']) + '/' + randomWord() + '.html'+hasTag('</' + elem.name + '>')+'\n';
	} else if (elem.type == 'list') {
		xml += tabs + hasTag('<' + elem.name + '>') + randomSyll(elem.list) + hasTag('</' + elem.name + '>')+'\n';
	} else if (elem.type == 'integer') {
		var min = parseInt(elem.min?elem.min:0);
		var max = parseInt(elem.max?elem.max:100);
		var val = (min+(max-min)*Math.random()).toFixed(0);
		xml += tabs + hasTag('<' + elem.name + '>') + val + hasTag('</' + elem.name + '>')+'\n';
	} else if (elem.type == 'float') {
		var min = parseFloat(elem.min?elem.min:0);
		var max = parseFloat(elem.max?elem.max:100);
		var val = (min+(max-min)*Math.random()).toFixed(2);
		xml += tabs + hasTag('<' + elem.name + '>') + val + hasTag('</' + elem.name + '>')+'\n';
	} else if (elem.type == 'date' ) {
		xml += tabs + hasTag('<' + elem.name + '>') + Math.round(2000+16*Math.random()) + '-' + Math.round(12*Math.random()) + '-' + Math.round(28*Math.random()) + hasTag('</' + elem.name + '>')+'\n';
	} else if (elem.type == 'group' ) { 
		var idx = config.indexOf(elem.group);
		if (idx > 0) {
			xml += hasTag(tabs + '<' + (elem.name || config[idx]) + '>\n');
			xml += createXMLComplexElement(tabs + '	', idx);
			xml += hasTag(tabs + '</' + (elem.name || config[idx]) + '>\n');
		} else {
			xml += tabs + '<\!-- unknown group tag ' + elem.name + '->' + elem.group + ' --\>\n';
		}
	} else xml += tabs + '<\!-- unknown tag ' + elem.name + '->' + elem.type + ' --\>\n';
	return xml;
}

function randomSyll(s) {
	var sylls = s || ['a', 'ing', 'er', 'ly', 'ed', 'i', 'es', 're', 'tion', 'in', 'e', 'con', 'y', 'ter', 'ex', 'al', 'de', 'com', 'di', 'en', 'an', 'ty', 'ry', 'u', 'ti', 'ri', 'be', 'per', 'to', 'pro', 'ac', 'ad', 'ar', 'ers', 'ment', 'or', 'tions', 'ble', 'der', 'ma', 'na', 'si', 'un', 'at', 'dis', 'ca', 'cal', 'man', 'ap', 'po', 'sion', 'vi', 'el', 'est', 'la', 'lar', 'pa', 'ture', 'for', 'is', 'mer', 'pe', 'ra', 'so', 'ta', 'as', 'col', 'fi', 'ful', 'get', 'low', 'ni', 'par', 'son', 'tle', 'day', 'ny', 'pen', 'pre', 'tive', 'car', 'ci', 'mo', 'an', 'aus', 'pi', 'se', 'ten', 'tor', 'ver', 'ber', 'can', 'dy', 'et', 'it', 'mu', 'no', 'ple', 'cu', 'fac', 'fer', 'gen', 'ic', 'land', 'light', 'o'];
	return sylls[Math.round(Math.random() * (sylls.length - 1))];
}

function randomWord(m,n) {
	var cnt = Math.round((m||1) + Math.random() * (n||3));
	var word = '';
	while (cnt--)
		word += randomSyll();
	return word;
}

function randomPhrase(n) {
	var cnt = (n||1)*Math.round(1 + Math.random() * 2);
	var phrase = '';
	while (cnt--)
		phrase += randomWord() + ' ';
	return phrase.trim();
}

function localSave(filename, text) {
	var blob = new Blob([text], {
			type : "text/plain;charset=utf-8"
		});
	saveAs(blob, filename);
}


function saveEmptyMetadata() {
	var xml = createXML();

	localSave(config[0] + '.xml', xml);
}

function saveSampleXML() {
	var xml = createXML(true);

	localSave(config[0] + '-sample.xml', xml);
}

function onRename(id)
{
	var textElem = document.getElementById('N'+id);
	var editElem = document.getElementById('I'+id);
	
	textElem.style.display = 'none';
	editElem.style.display = 'inline-block';
	editElem.focus();
	editElem.setSelectionRange(0, editElem.value.length);
}

function onRenameGroup(id)
{
	onRename(id);
	// TODO fix references to the renamed group
}

function onBlur(id)
{
	var textElem = document.getElementById('N'+id);
	var closeElem = document.getElementById('M'+id);
	var editElem = document.getElementById('I'+id);
	
	console.assert(textElem);
	console.assert(editElem); //closeElem could be undefined

	textElem.style.display = 'inline';
	editElem.style.display = 'none';
	
	var value = editElem.value.replace(/\W/g, '');
	textElem.innerHTML = value;
	editElem.value = value;
	if (closeElem) closeElem.innerHTML = value;
	for (var i=0; i<config.length; i++)
		if (config[i] instanceof Object)
			if (config[i].id==id)
			{
				config[i].name = value;
				break;
			}
			
	if (id==rootId)
	{
		document.getElementById('rootTag').innerHTML = value;
	}
}

function onBlurGroup(id)
{
	var textElem = document.getElementById('N'+id);
	var editElem = document.getElementById('I'+id);
	
	console.assert(textElem);
	console.assert(editElem);

	textElem.style.display = 'inline';
	editElem.style.display = 'none';
	
	var value = editElem.value.replace(/\W/g, '');
	var oldValue = textElem.innerHTML;
	
	editElem.value = value;
	textElem.innerHTML = value;

	var idx = config.indexOf(oldValue);
	console.assert(idx>=0);
	
	var oldName = config[idx];
	config[idx] = value;

	// fix references to the renamed group
	if (oldName != value)
		for (var i=0; i<config.length; i++)
		{
			if (config[i] instanceof Object)
			{
				//console.log(i,config[i].type,config[i].group);
				if (config[i].type=='group')
				{
					//console.log(config[i].group,groupName);
					if (config[i].group == oldName)
					{
						config[i].group = value;
						document.getElementById('GR'+config[i].id).value = value;
						document.getElementById('GR'+config[i].id).innerHTML+='<option value="'+value+'" selected="selected">'+value+'</option>';
						//onChangedType(config[i].id);
					}
				}
			}
		}
}

function onBlurNoSave(id)
{
	var textElem = document.getElementById('N'+id);
	var editElem = document.getElementById('I'+id);
	
	textElem.style.display = 'inline';
	editElem.style.display = 'none';
}

function onDelete(id)
{
	var rowElem = document.getElementById('R'+id);
	console.assert(rowElem);
	
	rowElem.parentNode.removeChild(rowElem);
	for (var i=0; i<config.length; i++)
		if (config[i] instanceof Object)
			if (config[i].id==id)
			{
				config.splice(i,1);
				break;
			}
}

function onDeleteGroup(id)
{
	var frameElem = document.getElementById('F'+id);
	var inputElem = document.getElementById('I'+id);
	console.assert(frameElem);
	
	frameElem.parentNode.removeChild(frameElem);
	var groupName = inputElem.value;
	var idx = config.indexOf(groupName);
	console.assert(idx>=0);
	
	config.splice(idx,1);
	while ((config[idx] instanceof Object) && idx<config.length)
		config.splice(idx,1);
		
	// remove all references to deleted group
	for (var i=0; i<config.length; i++)
	{
		if (config[i] instanceof Object)
		{
			//console.log(i,config[i].type,config[i].group);
			if (config[i].type=='group')
			{
				//console.log(config[i].group,groupName);
				if (config[i].group == groupName)
				{
					config[i].group = '';
					document.getElementById('GR'+config[i].id).value = '';
					//onChangedType(config[i].id);
				}
			}
		}
	}
}

function onKeyDown(event)
{
	var id = event.target.id.replace('I','');
	var textElem = document.getElementById('N'+id);
	var editElem = document.getElementById('I'+id);
	
	if (event.key=='Escape')
	{
		onBlurNoSave(id);
		editElem.value = textElem.innerHTML;
	}
	
	if (event.key=='Enter')
	{
		onBlur(id);
		event.stopPropagation();
	}
}

function onKeyDownGroup(event)
{
	var id = event.target.id.replace('I','');
	var textElem = document.getElementById('N'+id);
	var editElem = document.getElementById('I'+id);
	
	if (event.key=='Escape')
	{
		onBlurNoSave(id);
		editElem.value = textElem.innerHTML;
	}
	
	if (event.key=='Enter')
	{
		onBlurGroup(id);
		event.stopPropagation();
	}
}

function onFocusType(id)
{
	var typeElem = document.getElementById('GR'+id);
	console.assert(typeElem);
	
	var value = typeElem.value;
	var html = '';
	
	for (var i=1; i<config.length; i++)
		if (!(config[i] instanceof Object))
			html += '<option value="'+config[i]+'">'+config[i]+'</option>';

	var different = typeElem.innerHTML!=html;
	
	typeElem.innerHTML = html;
	typeElem.value = value;
	
	if (different)
	{
		typeElem.blur();
		typeElem.focus();
	}
}

function optionHTML(value,text,selectedValue)
{
	var html = '<option value="'+value+'"';
	if (value==selectedValue) html += ' selected="selected"';
	html += '>'+text+'</option>';
	return html;
}

function findConfigById(id)
{
	for (var i=0; i<config.length; i++)
		if (config[i] instanceof Object)
			if (config[i].id==id)
				return i;
	return -1;
}

function onChangedOccurs(id)
{
//console.log('onChangedOccurs('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('OC'+id);
	console.assert( elem );

	config[i].occurs = elem.value;
}

function onChangedType(id)
{
//console.log('onChangedType('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('TY'+id);
	console.assert( elem );

	// show/hide fields depending on the type
	document.getElementById('xLI'+id).style.display = (elem.value=='list')?'table-cell':'none';
	document.getElementById('xGR'+id).style.display = (elem.value=='group')?'table-cell':'none';
	document.getElementById('xHT'+id).style.display = (elem.value=='text'||elem.value=='paragraph'||elem.value=='url')?'table-cell':'none';
	document.getElementById('xWI'+id).style.display = (elem.value=='text'||elem.value=='paragraph'||elem.value=='date'||elem.value=='url'||elem.value=='integer'||elem.value=='float')?'table-cell':'none';
	document.getElementById('xHE'+id).style.display = (elem.value=='paragraph')?'table-cell':'none';
	document.getElementById('xMIN'+id).style.display = (elem.value=='integer'||elem.value=='float')?'table-cell':'none';
	document.getElementById('xMAX'+id).style.display = (elem.value=='integer'||elem.value=='float')?'table-cell':'none';
	
	config[i].type = elem.value;
}

function onChangedGroup(id)
{
//console.log('onChangedGroup('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('GR'+id);
	console.assert( elem );

	config[i].group = elem.value;
}

function onChangedList(id)
{
//console.log('onChangedList('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('LI'+id);
	console.assert( elem );

	config[i].list = elem.value.split(',');
}

function onChangedLabel(id)
{
//console.log('onChangedLabel('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('LB'+id);
	console.assert( elem );

	config[i].label = elem.value;
}

function onChangedHint(id)
{
//console.log('onChangedHint('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('HT'+id);
	console.assert( elem );

	config[i].hint = elem.value;
}

function onChangedMin(id)
{
//console.log('onChangedHint('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('MIN'+id);
	console.assert( elem );

	config[i].min = elem.value;
}

function onChangedMax(id)
{
//console.log('onChangedHint('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('MAX'+id);
	console.assert( elem );

	config[i].max = elem.value;
}

function onChangedTooltip(id)
{
//console.log('onChangedTooltip('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('TT'+id);
	console.assert( elem );

	config[i].tooltip = elem.value;
}

function onChangedWidth(id)
{
//console.log('onChangedWidth('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('WI'+id);
	console.assert( elem );

	config[i].width = elem.value;
}

function onChangedHeight(id)
{
//console.log('onChangedHeight('+id+')');

	var i = findConfigById(id);
	console.assert( i>=0 );
	
	var elem = document.getElementById('HE'+id);
	console.assert( elem );

	config[i].height = elem.value;
}

function onShowMore(id)
{
	var novip = document.getElementById('NV'+id);
	var more = document.getElementById('SM'+id);
	var less = document.getElementById('SL'+id);

	novip.style.display = 'block';
	more.style.display = 'none';
	less.style.display = 'inline-block';
}

function onShowLess(id)
{
	var novip = document.getElementById('NV'+id);
	var more = document.getElementById('SM'+id);
	var less = document.getElementById('SL'+id);

	novip.style.display = 'none';
	more.style.display = 'inline-block';
	less.style.display = 'none';
}

function createRow(elem,hasDelete)
{
	var id = elem.id;
	
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id','R'+id);
	
	// opening tag + input field
	var name = elem.name||elem.group;
	var html = '&lt;<span id="N'+id+'">'+name+'</span><input type="text" style="display:none;" value="'+name+'" id="I'+id+'" onblur="onBlur('+id+')" onkeydown="onKeyDown(event)">&gt;';

	// buttons
	html += '<span class="autohiderow">';
	html += ' <span class="textButton tooltip" href="#" onclick="onRename('+id+')">Rename<span class="tooltiptext">Rename '+(hasDelete?'this':'the main')+' tag</span></span>';
	if (hasDelete)
	{
		html += ' <span class="textButton tooltip" href="#" onclick="onDelete('+id+')">Delete<span class="tooltiptext">Delete just this tag</span></span>';
	}
	html += '</span>';
	html += '<br>';
	
	// vip contents
	//html += '<div class="vip">';
	html += '<table class="vip"><tr>';
	{
		if (hasDelete)
			html += '<td>';
		else
			html += '<td style="display:none">';
		html += '<span class="note">Instances:</span>';
		// selector for number of instances
		html += '<select onchange="onChangedOccurs('+id+')" id="OC'+id+'">';
		html += optionHTML('11','Exactly one',elem.occurs);
		if (hasDelete)
		{
			html += optionHTML('01','Zero or one',elem.occurs);
			html += optionHTML('0u','Any number',elem.occurs);
			html += optionHTML('1u','One or more',elem.occurs);
		}
		html += '</select>';
		html += '</td>';
		
		// selector of type
		html += '<td>';
		html += '<span class="note">Contains:</span>';
		var type = elem.type;
		console.assert(knownTypes.indexOf(type)>=0);
		html += '<select id="TY'+id+'" onchange="onChangedType('+id+')">';
		html += optionHTML('text','Text',type);
		html += optionHTML('paragraph','Paragraph',type);
		html += optionHTML('integer','Integer',type);
		html += optionHTML('float','Float',type);
		html += optionHTML('url','URL',type);
		html += optionHTML('date','Date',type);
		html += optionHTML('list','List',type);
		html += optionHTML('group','Nested tags',type);
		html += '</select>';
		html += '</td>';
		
		// textbox of list
		html += '<td id="xLI'+id+'">';
		html += '<span class="note">Comma separated values:</span>';
		html += '<input type="text" id="LI'+id+'" ';
		if (type=='list')
			html += 'value="'+elem.list.join(',')+'"';
		else
			html += 'value=""';
		html += ' onchange="onChangedList('+id+')">';
		html += '</td>';

		// selector of group
		html += '<td id="xGR'+id+'" style="min-width:5.5em;">';
		html += '<span class="note">Defined in group:</span>';
		html += '<select id="GR'+id+'" onfocus="onFocusType('+id+')" onchange="onChangedGroup('+id+')">';
		if (type=='group')
			html += '<option value="'+elem.group+'" selected="selected">'+elem.group+'</option>';
		html += '</select>';
		html += '</td>';
		
		// add button for showing/hiding no vip
		html += '<td style="width:100%; text-align: right;">';
		html += ' <span class="textButton tooltip" id="SM'+id+'" style="font-size:40%;" href="#" onclick="onShowMore('+id+')">More<span class="tooltiptext" style="font-size:100%;">Show more properties</span></span>';
		html += ' <span class="textButton tooltip" id="SL'+id+'" style="font-size:40%; display:none;" href="#" onclick="onShowLess('+id+')">Less<span class="tooltiptext" style="font-size:100%;">Show less properties</span></span>';
		html += '</td>';
	}
	// end of vip contents
	//html += '</div>';
	html += '</tr></table>';
	
	// novip contents
	html += '<table class="novip" id="NV'+id+'">';
	{
		html += '<tr>';
		// label
		html += '<td colspan="3">';
		html += '<span class="note">Label:</span>';
		html += '<input type="text" id="LB'+id+'" ';
		html += 'value="'+(elem.label||'')+'" ';
		html += 'onchange="onChangedLabel('+id+')">';
		html += '</td>';

		// tooltip
		html += '<td colspan="3">';
		html += '<span class="note">Pop-up tooltip:</span>';
		html += '<input type="text" id="TT'+id+'" ';
		html += 'value="'+(elem.tooltip||'')+'" ';
		html += 'onchange="onChangedTooltip('+id+')">';
		
		html += '</td>';
		html += '</tr>';

		// hint
		// html += '<td id="xHT'+id+'" colspan="3">';
		// html += '<span class="note">In-line hint:</span>';
		// html += '<input type="text" id="HT'+id+'" ';
		// html += 'value="'+(elem.hint||'')+'" ';
		// html += 'onchange="onChangedHint('+id+')">';
		// html += '</td>';

		html += '</tr>';

		html += '<tr>';

		// tooltip
		// html += '<td colspan="3">';
		// html += '<span class="note">Pop-up tooltip:</span>';
		// html += '<input type="text" id="TT'+id+'" ';
		// html += 'value="'+(elem.tooltip||'')+'" ';
		// html += 'onchange="onChangedTooltip('+id+')">';
		// html += '</td>';

		// hint
		html += '<td id="xHT'+id+'" colspan="3">';
		html += '<span class="note">In-line hint:</span>';
		html += '<input type="text" id="HT'+id+'" ';
		html += 'value="'+(elem.hint||'')+'" ';
		html += 'onchange="onChangedHint('+id+')">';
		html += '</td>';

		// min
		html += '<td id="xMIN'+id+'">';
		html += '<span class="note">Min value:</span>';
		html += '<input type="text" size="6" id="MIN'+id+'" ';
		html += 'value="'+(elem.min||'')+'" ';
		html += 'onchange="onChangedMin('+id+')">';
		html += '</td>';

		// max
		html += '<td id="xMAX'+id+'" colspan="2">';
		html += '<span class="note">Max value:</span>';
		html += '<input type="text" size="6" id="MAX'+id+'" ';
		html += 'value="'+(elem.max||'')+'" ';
		html += 'onchange="onChangedMax('+id+')">';
		html += '</td>';

		// width
		html += '<td id="xWI'+id+'">';
		html += '<span class="note">CSS width:</span>';
		html += '<input type="text" size="6" id="WI'+id+'" ';
		html += 'value="'+(elem.width||'')+'" ';
		html += 'onchange="onChangedWidth('+id+')">';
		html += '</td>';
		
		// height
		html += '<td id="xHE'+id+'">';
		html += '<span class="note">CSS height:</span>';
		html += '<input type="text" size="6" id="HE'+id+'" ';
		html += 'value="'+(elem.height||'')+'" ';
		html += 'onchange="onChangedHeight('+id+')">';
		html += '</td>';
		
		html += '</tr>';
	html += '</table>';
	}
	// end of vip contents
	

	// closing tag
	html += '&lt;/<span id="M'+id+'">'+name+'</span>&gt;';
	row.innerHTML = html;
	
	return row;
}

function addTag(id)
{
	var frame = document.getElementById('F'+id);
	console.assert(frame);

	var name = document.getElementById('I'+id).value;

	var idx = config.indexOf(name);
	console.assert(idx>=0);
	if (idx<0) console.error('Cound not find "'+name+'" in config[]');
	idx++;
	
	// go to the end of the group
	while ((idx<config.length) && (config[idx] instanceof Object)) idx++;
	
	window.id++
	config.splice(idx,0,{name:randomWord(2,2), type:'text', occurs:'11'});
	var elem = config[idx];
	elem.id = window.id;
	var row = createRow(elem,true);
	frame.insertBefore(row,frame.lastChild);
	onRename(elem.id);
	onChangedOccurs(elem.id);
	onChangedType(elem.id);
}

function addNewGroup()
{
	id++;
	config.push(randomWord(3,3));
	var frame = addGroup(config.length-1);
	onRenameGroup(id);
}

function addGroup(i)
{
	var first = i==0;
	var elem = config[i];
	
	id++;
	var frame = document.createElement('div');
	frame.className = 'frame';
	frame.setAttribute('id','F'+id);
	
	var html = '';
	// group name
	html = '<span class="framename">'+(first?'File':'Group')+': <span id="N'+id+'">'+elem+'</span><input type="text" size="10" style="display:none;" id="I'+id+'" value="'+elem+'"'+ 'onblur="onBlurGroup('+id+')" onkeydown="onKeyDownGroup(event)"></span>';
	
	// group buttons - top
	html += '<span class="autohideframe" style="float:right; margin-right:0.6em;">';
	html += ' <span class="textButton tooltip" href="#" onclick="onRenameGroup('+id+')">Rename<span class="tooltiptext">Rename the '+(first?'file':'group')+'</span></span>';
	if (!first)
	{
		html += ' <span class="textButton tooltip" href="#" onclick="onDeleteGroup('+id+')">Delete<span class="tooltiptext">Delete the whole group. And all references to it. Forever!</span></span>';
	}
	html += '</span>';
	
	// group buttons - bottom
	if (!first)
	{
		html += '<br><span><span class="textButton tooltip autohideframe" href="#" onclick="addTag('+id+')">+ &lt;...&gt;<span class="tooltiptext">Add a new tag here</span></span>';
	}
	else
	{
		html += '<div class="note footnote"><i>&nbsp;&ndash; Note: &lt;<span id="rootTag">This</span>&gt; is the main tag.</i></div>';
	}
	
	frame.innerHTML = html;
	
	var frames = document.getElementById('frames');
	var bar = document.getElementsByClassName('bar')[0];
	frames.insertBefore(frame,bar);
	return frame;
}

var id = 10000;
var rootId = 0;

function fillFrames()
{
	rootId = 0;
	var frames = document.getElementById('frames');
	if (!frames)
	{
		console.error('No frames hosting tag "frames"');
		return;
	}
	
	frames.innerHTML = '<hr class="bar">';
	for (var i=0; i<config.length; i++)
	{
		var elem = config[i];
		if (elem instanceof Object)
		{
			id++;
			elem.id = id;
			var row = createRow(elem,i>1);
			frame.insertBefore(row,frame.lastChild);
			if (!rootId)
			{
				rootId = id;
				document.getElementById('rootTag').innerHTML = elem.name;
			}
		}
		else {
			frame = addGroup(i);
		}
	}

	// add a frame-button for adding new frames
	
	var frame = document.createElement('div');
	frame.className = 'frame tooltip';
	var html = '+ Group<span class="tooltiptext">Add a new group of metadata values</span>';
	frame.innerHTML = html;

	// fix visibility of all elements
	for (var i=0; i<config.length; i++)
		if (config[i] instanceof Object)
		{
			onChangedType(config[i].id);
		}

	frame.onclick = addNewGroup;
	frames.appendChild(frame);
}

function newConfig()
{
	config = [
		randomWord(2,2), //filenames
			{name:randomWord(2,2), type:'text'}
	];
	fillFrames();
}

function saveConfig()
{
	var json = JSON.stringify(config);
	localSave(config[0] + '.json', json);
}

function loadConfig()
{
	var fileSelect = document.getElementById('selectFileJS');
	fileSelect.onchange = onJSONFileSelect;
	fileSelect.setAttribute('accept', '.json');
	fileSelect.click();
}

function onJSONFileSelect(event) {
	//console.table(event.target.files);
	if (event.target.files.length < 1)
		return;

	// take only the first file
	var file = event.target.files[0]; 
	
	// exit if the file extension is not json
	if (file.name.slice(-5)!='.json') return;

	
	var reader = new FileReader();

	reader.onload = function (e) {
		config = JSON.parse(e.target.result);
		fillFrames();
	};

	reader.readAsText(file);
	document.getElementById('selectFileJS').value = '';
}

function boo(xml)
{
	console.log('boo',xml);
}

function onChangeInteger(event)
{
	var input = event.target;
	var value = input.value.trim();
	if (value==='') return;
	
	value = parseInt(value);
	
	if (input.hasAttribute('min'))
	{
		var limit = parseInt(input.getAttribute('min'));
		if (value<limit) value = limit;
	}
	
	if (input.hasAttribute('max'))
	{
		var limit = parseInt(input.getAttribute('max'));
		if (value>limit) value = limit;
	}
	
	input.value = value;
}

function onChangeFloat(event)
{
	var input = event.target;
	var value = input.value.trim();
	if (value==='') return;
	
	value = parseFloat(value);
	
	if (input.hasAttribute('min'))
	{
		var limit = parseInt(input.getAttribute('min'));
		if (value<limit) value = limit;
	}
	
	if (input.hasAttribute('max'))
	{
		var limit = parseInt(input.getAttribute('max'));
		if (value>limit) value = limit;
	}
	
	input.value = value;
}

function generateHTML(i,data,showRemove)
{
	var html = '';
	
	var star = '';
	
	//console.log('i=',i);
	//console.log('data=',data);
	//console.log('config['+i+']=',config[i]);
	var occurs = (typeof config[i].occurs === 'undefined')?'11':config[i].occurs;
	if (occurs[0]!='0' )
		star = '<span class="compulsory">*</span>';

	var pureLabel = (config[i].label || config[i].name);
	var label = pureLabel+star;

	//console.log(pureLabel,config[i].occurs,occurs);
	
	var style = '';
	if (config[i].width || config[i].height)
	{
		style = ' style="';
		if (config[i].width) style += 'width:'+config[i].width+'; ';
		if (config[i].height) style += 'height:'+config[i].height+'; ';
		style += '"';
	}
	
	var add = '';
	if (occurs[1]!='1' )
	{
		add += '<div class="add" onclick="clone(event)" style="display:inline-block;"> + add '+pureLabel+'</div>';
		add += '<div class="remove" onclick="remove(event)" style="display:'+(showRemove?'inline-block':'none')+';"> - remove</div>';
	}
	
	if (add && config[i].type!='group') add = '<br>'+add;
	
	var startTag = '	<span class="generate" cmd="open">&lt;'+config[i].name+'&gt;</span>';
	var endTag = '	<span class="generate" cmd="close">&lt;/'+config[i].name+'&gt;</span>';
	var valueTag = '	<span class="generate" cmd="value"></span>';
	var indentTag = '	<span class="generate" cmd="indent"></span>';
	var unindentTag = '	<span class="generate" cmd="unindent"></span>';

	var value = data?(data.innerHTML||data.textContent):'';
	var hint = config[i].hint || '';

	function htmlLabel(className)
	{
		if (config[i].tooltip)
			return '	<span class="'+className+' tooltip">'+label+'<span class="tooltiptext">'+config[i].tooltip+'</span></span><br>';
		else
			return '	<span class="'+className+'">'+label+'</span><br>';
	}
	
	switch (config[i].type)
	{
		case 'group':
			html += '<div class="group">';
			html += startTag+indentTag;
			html += htmlLabel('mainlabel');
			html += '   <div class="contents">';
			var idx = config.indexOf(config[i].group);
			if (idx < 0)
				html += '<div class="error">Unknown group "'+config[i].group+'"</div>';
			else
			{
				var ii = 0; // index of XML data
				var children = [];
				if (data)
				{
					for (var j=0; j<data.childNodes.length; j++)
						if (data.childNodes[j].nodeType==1)
					 		children.push(data.childNodes[j]);
				}
				while (config[++idx] instanceof Object)
				{
					var generated = false;
					while (ii<children.length && config[idx].name==children[ii].localName)
					{
						html += generateHTML(idx,children[ii],generated);
						ii++;
						generated = true;
					}
					if (!generated) html += generateHTML(idx);
				}
			}
			html += '</div>';
			html += add;
			html += unindentTag+endTag;
			html += '</div>';
			break;
			
		case 'text':
			html += '<div class="text">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			html += '	<input type="text"'+style+' id="'+i+'" value="'+value+'" placeholder="'+hint+'">';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		case 'url':
			html += '<div class="url">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			html += '	<input type="text"'+style+' id="'+i+'" value="'+value+'" placeholder="'+hint+'">';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		case 'date':
			html += '<div class="date">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			html += '	<input type="date"'+style+' id="'+i+'" value="'+value+'">';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		case 'integer':
			html += '<div class="integer">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			var min = '';
			var max = '';
			if (config[i].min) min = ' min="'+parseInt(config[i].min)+'"';
			if (config[i].max) max = ' max="'+parseInt(config[i].max)+'"';
			html += '	<input type="number"'+style+' id="'+i+'" value="'+value+'"'+min+max+' step="1" onchange="onChangeInteger(event);">';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		case 'float':
			html += '<div class="float">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			var min = '';
			var max = '';
			if (config[i].min) min = ' min="'+config[i].min+'"';
			if (config[i].max) max = ' max="'+config[i].max+'"';
			html += '	<input type="number"'+style+' id="'+i+'" value="'+value+'"'+min+max+' onchange="onChangeFloat(event);">';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		case 'paragraph':
			html += '<div class="paragraph">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			html += '	<textarea '+style+' id="'+i+'">'+value+'</textarea>';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		case 'list':
			html += '<div class="list">';
			html += startTag;
			html += htmlLabel('label');
			html += valueTag;
			html += '	<select '+style+' id="'+i+'">';
			for (var j=0; j<config[i].list.length; j++)
				html += '	<option value="'+config[i].list[j]+'" '+(config[i].list[j]==value?'selected':'')+'>'+config[i].list[j]+'</option>';
			html += '	</select>';
			html += add;
			html += endTag;
			html += '</div>';
			break;
			
		default:
			html += '<div class="error">Unknown type "'+config[i].type+'"</div>';
	}
		
	if (config[i].type=='paragraph') html += '<br>';
	
	return html;
}

function fillDataFrames()
{
	var html = generateHTML(1,/*data.firstElementChild*/ data.childNodes[0]);
	var frames = document.getElementById('frames');
	frames.innerHTML = html;
}

function clone(event)
{
	var oldBlock = event.target.parentNode;
	var newBlock = oldBlock.cloneNode(true);
	oldBlock.parentNode.insertBefore(newBlock, oldBlock.nextSibling);
	var buttons = newBlock.querySelectorAll(".remove");
	for (var i=0; i<buttons.length; i++)
	  if (buttons[i].parentNode==newBlock) buttons[i].style.display="inline-block";
}
	
function remove(event)
{
	var block = event.target.parentNode;
	block.parentNode.removeChild(block);
}
	
function loadXMLdata()
{
	var fileSelect = document.getElementById("selectFileJS");
	fileSelect.onchange = onXMLFileSelect;
	fileSelect.setAttribute('accept', '.xml');
	fileSelect.click();
}

function onXMLFileSelect(event)
{
	if (event.target.files.length < 1) return;
	var file = event.target.files[0]; 
	if (file.name.slice(-4)!=".xml") return;
	var reader = new FileReader();
	reader.onload = function (e) {
		parser = new DOMParser();
		xmlDoc = parser.parseFromString(e.target.result,"text/xml");
		loadXML(xmlDoc);
	};
	reader.readAsText(file);
	document.getElementById("selectFileJS").value = "";
}

function loadJSONdata()
{
	var fileSelect = document.getElementById("selectFileJS");
	fileSelect.onchange = onJSONFileSelectData;
	fileSelect.setAttribute('accept', '.json');
	fileSelect.click();
}

function objectToXml(obj,forceName) {
        var xml = '';

        for (var prop in obj) {
            if (!obj.hasOwnProperty(prop)) {
                continue;
            }

            if (obj[prop] == undefined)
                continue;
			
			var tag = forceName?forceName:prop;
			
            if (Array.isArray(obj[prop]))
                xml += objectToXml(new Object(obj[prop]),prop);
			else
			{
				xml += "<" + tag + ">";
				if (typeof obj[prop] == "object")
					xml += objectToXml(new Object(obj[prop]));
				else
					xml += obj[prop];

				xml += "</" + tag + ">";
			}
        }

        return xml;
    }
	
function onJSONFileSelectData(event)
{
	if (event.target.files.length < 1) return;
	var file = event.target.files[0]; 
	if (file.name.slice(-5)!=".json") return;
	var reader = new FileReader();
	reader.onload = function (e) {
		var obj = JSON.parse(e.target.result);
		var xml = objectToXml(obj);
		xmlDoc = parser.parseFromString(xml,"text/xml");
		loadXML(xmlDoc);
	};
	reader.readAsText(file);
	document.getElementById("selectFileJS").value = "";
}

function loadXML(xml)
{
	data = xml;
	fillDataFrames();
}



function generateXML(niceFormat)
{
	var xml = "";
	var spc = "";
	var gen = document.querySelectorAll(".generate");
	for (var i=0; i<gen.length; i++)
	{
		var cmd = gen[i].getAttribute("cmd");
		if (niceFormat && i && cmd=="open") xml += String.fromCharCode(13)+spc;
		if (cmd=="open" || cmd=="close") xml += gen[i].innerHTML.replace("&lt;","<").replace("&gt;",">");
		if (cmd=="value") xml += gen[i].nextSibling.nextSibling.value;
		if (niceFormat && cmd=="unindent") {spc=spc.slice(0,-2); xml += String.fromCharCode(13)+spc;}
		if (niceFormat && cmd=="indent") spc+="  ";
	}
	return xml;
}

function saveXML(event)
{
	var xml = generateXML(true);
	localSave(config[0]+'.xml', xml);
}

// Changes XML to JSON
// from: https://davidwalsh.name/convert-xml-json
function xmlToJson(xml) {
	
	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	var nodeName;
	if (xml.hasChildNodes()) {
		
		if (xml.childNodes.length==1)
			if (xml.childNodes.item(0).nodeType==3)
		{
			return (xml.childNodes.item(0).nodeValue);
		}
		
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);

			nodeName = item.nodeName;

			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};

function saveXMLJSON(event)
{
	var xml = generateXML(false);
//	console.log('xml=',xml);
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(xml,"text/xml");
//	console.log('xmldoc=',xmlDoc);
	var obj = xmlToJson(xmlDoc);
//	console.log('obj=',obj);
	var json = JSON.stringify(obj);
//	console.log('json=',json);
	localSave(config[0]+'.json', json);
}

function saveMetadataEditor() 
{
	var html = '<!DOCTYPE html>\n';

	document.getElementById('json-buttons').style.display = 'none';
	html += document.documentElement.outerHTML;
	document.getElementById('json-buttons').style.display = 'block';
	
	var i = html.split('<div class="cutmarker">'+'</div>');
	html = i[0]+'<div id="frames"></div>'+i[2];
	
	html = html.replace("json = ''; "+"//SPECIAL MARKER","json = '"+JSON.stringify(config)+"'; "+"//SPECIAL MARKER");
	localSave(config[0]+'.html', html);
}

